(function() {
	'use strict';

	/*
	* Controller to manage the Simple Bar chart widget.
	* link:https://www.predix-ui.com/?show=px-simple-bar-chart&type=component
	*/
	function PxSimpleBarChartCtrl($scope, $timeout, $element){
		var ctrl								= this;
		var	target								= $scope.DECISYON.target;
		var	ctx									= target.content.ctx;
		var	refObjId							= ctx.refObjId.value;
		var	unbindWatches						= [];
		var	OVERFLOW_BUG_FIX					= 5;

		/* Managing of line chart Properties */
		var manageParamsFromContext = function(context) {
			//SELECT PROPERTIES
			ctrl.legendOrder		= context.$pxBarChartLegendOrder.value.value;
			//HARD-CODED PROPERTIES
			ctrl.refObjId           = refObjId;
		};

		/* Inizialize chart*/
		var inizializeChart = function(data, ctx) {
			manageParamsFromContext(ctx);
			attachListeners(ctx);
			applyDimensionsToWebComponent(ctx);
			setData(data);
		};

		/* Update chart for context parameters and dimesion*/
		var updateChart = function(data, ctx) {
			setData(data);
		};

		/* Populate the data fetched through controller */
		var setData = function(data) {
		    ctrl.barData    = data[0]; 
			ctrl.currentTpl = 'simpleBarChart.html';
		};

		/* Routine called when angular element is destroied*/
		var destroyWidget = function() {
			for (var i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};

		/* Function to handle DC failure */
		var setChartInError = function(rejection) {
			ctrl.errorMsg = rejection.errorMsg;
			ctrl.currentTpl = 'error.html';
			destroyWidget();
		};

		/* Get data from the Connector */
		var getData = function(ctx) {
			//The widget is in dashboard; prevent abnormal behavior into preview
			if (target.page){
				if (angular.equals(ctx.useDataConnector.value,'true')){
					var dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function(data) {
							dataConnector.instance.data.then(function success(data) {
								updateChart(data, ctx);
							},function error(rejection) {
								setChartInError(rejection);
							});
						});
						dataConnector.instance.data.then(function success(data) {
							inizializeChart(data, ctx);
						},function error(rejection) {
							setChartInError(rejection);
						});
					});
				} else {
					ctrl.barData  = [];
				}
			}else {
				setChartInError({errorMsg : 'You are not on mashboard page. The widget can\'t be inizialized.'});
			}
		};
		/* It adjust size ad fix overflow*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace(/%/g, '').trim();
			return (parseInt(adjustedSize) > OVERFLOW_BUG_FIX) ? (parseInt(size) - OVERFLOW_BUG_FIX) : adjustedSize;
		};

		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
			Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/
		var applyDimensionsToWebComponent = function(ctx) {
			ctrl.width    = getAdjustedSizing(ctx['min-width'].value);
			ctrl.height   = getAdjustedSizing(ctx['min-height'].value);
		};

		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};
        
		//First instruction to get the data
		getData(ctx);
	}
	PxSimpleBarChartCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxSimpleBarChartCtrl', PxSimpleBarChartCtrl);
}());